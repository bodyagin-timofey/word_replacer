﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private List<String> items = new List<string>();
        private String xml;
        private String path;

        public Form1()
        {
            InitializeComponent();
        }

        private void btOpen_Click(object sender, EventArgs e)
        {
            of.FileName = null;
            DialogResult dr = of.ShowDialog();
            if (of.FileName != "") {
                path = of.FileName;
                OpenFile();
            }

        }

        private void OpenFile() {
            items.Clear();
            using (ZipFile zip = ZipFile.Read(path))
            {
                ZipEntry e = zip["word/document.xml"];
                using (var ms = new MemoryStream())
                {
                    e.Extract(ms);
                    ms.Position = 0;
                    var sr = new StreamReader(ms);
                    xml = sr.ReadToEnd();
                    int lastStart = 0;
                    int start = xml.IndexOf("{", lastStart+1);
                    while (start > 0) {
                        int end = -1;
                        if (start >= lastStart)
                        {
                            end = xml.IndexOf("</w:t>", start);
                            while (end > 0 && !xml[end - 1].Equals('}'))
                            {
                                int startDel = end;
                                int endDel = xml.IndexOf("<w:t>", startDel) + 5;
                                xml = xml.Substring(0, startDel) + xml.Substring(endDel);
                                end = xml.IndexOf("</w:t>", start);
                            }
                            items.Add(xml.Substring(start, end - start));
                        }
                        lastStart = start + 1;
                        start = xml.IndexOf("{", lastStart + 1);
                    }
                }
            }
            dg.RowCount = 0;
            List<String> tmp = new List<string>();
            foreach(String item in items){
                if(!tmp.Contains(item) && !item.Contains("Z")){
                    tmp.Add(item);
                    int rowIdx = dg.Rows.Add();
                    dg.Rows[rowIdx].Cells[0].Value = item;
                }
            }
        }

        private void btProcess_Click(object sender, EventArgs e)
        {
            sf.FileName = null;
            DialogResult dr = sf.ShowDialog();
            if (sf.FileName == "") {
                return;
            }
            for (int i = 0; i < dg.RowCount; i++) {
                String src = dg.Rows[i].Cells[0].Value.ToString();
                String dst = dg.Rows[i].Cells[1].Value == null ? "" : dg.Rows[i].Cells[1].Value.ToString();
                xml = xml.Replace(src, dst);
                if (src.Contains("X")) {
                    src = src.Replace("X", "Z");
                    xml = xml.Replace(src, dst);
                }
            }
            
            using (ZipFile zip = ZipFile.Read(path))
            {
                zip.RemoveEntry("word/document.xml");

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                zip.AddEntry("word/document.xml", stream);
                zip.Save(sf.FileName);
            }
//            System.Console.WriteLine(xml);
        }
    }
}
